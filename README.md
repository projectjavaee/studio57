_Project Studio 57_
===================

_***Технологии проекта:***_

1. _Spring-Boot;_
2. _Spring Security;_
3. _Maven;_
4. _Hibernate;_
5. _MySql;_
6. _Angular;_
7. _HTML5;_
8. _Bootstrap;_
9. _JQuery;_
10. _Tomcat;_
11. _Node.js;_
12. _и другие._

_***Для успешного скачивания и запуска проекта должно быть установлено следующее ПО:***_

1. _[JAVA 8](http://www.oracle.com/technetwork/java/javase/downloads/index.html "Страница загрузки JAVA");_
2. _[Apache Maven](https://maven.apache.org/ "Сайт Maven");_
3. _[Yeoman](http://yeoman.io/ "Сайт Yeoman") для устанановки необходимо в командной строке или терминале набрать команду ``npm install -g yo``;_
4. _[Node.js](https://nodejs.org/en/download/ "Страница загрузки Node.js") включая менеджер пакетов [npm](https://www.npmjs.com/ "Сайт npm");_
5. _[Git](https://git-scm.com/download "Страница загрузки Git");_
6. _[JHipster](http://www.jhipster.tech/ "Сайт JHipster") для установки необходимо в командной строке или терминале набрать команду ``npm install -g generator-jhipster``;_ 
7. _[MySQL](https://dev.mysql.com/downloads/ "Страница загрузки MySQL")._

##_Сборка проекта_
_***Для восстановления зависимостей необходимо с помощью командной строки или терминала перейти в корневую папку проекта, где расположен файл [package.json](package.json) и выполнить команду ``npm install``.***_

##_Создание базы данных_
_***Для создания базы данных небходимо в MySQL Command Line Client ввести команду ```create database studio57```, либо воспользоваться [Workbench](https://www.mysql.com/products/workbench/ "Страница Workbench"), либо командной строкой или терминалом. 
После в файле [application-dev.yml](src/main/resources/config/application-dev.yml) в полях ```spring:datasource:username``` и ```spring:datasource:password:``` указать учетные данные базы.***_

##_Запуск проекта_
_***Запуск проекта осуществляется стартом back-end, также в случае необходимости можно запустить front-end:***_

1. _Back-end: запустите метод main в классе [Studio57App.java](src/main/java/ru/ars/Studio57App.java), для этого после компиляции проекта с помощью командной строки или терминала перейдите в директорию ``target`` в папку скомпилированного класса ```Studio57App.class``` и введите команду ``java Studio57App``;_
2. _Front-end: с помощью командной строки или терминала перейти в корневую папку проекта, где расположен файл [package.json](package.json) и выполнить команду ``npm start``;_
3. _Откройте в браузере ссылку [http://localhost:8080/](http://localhost:8080/), в случае запуска и front-end проект можно открыть по ссылке [http://localhost:9000/](http://localhost:9000/)._
