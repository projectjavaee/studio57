/**
 * View Models used by Spring MVC REST controllers.
 */
package ru.ars.web.rest.vm;
