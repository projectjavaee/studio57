package ru.ars.repository;

import ru.ars.domain.Project;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import java.util.List;

/**
 * Spring Data JPA repository for the Project entity.
 */
@Repository
@SuppressWarnings("unused")
public interface ProjectRepository extends JpaRepository<Project, Long> {
    @Query("select distinct p from Project p left join fetch p.users")
    List<Project> findAllWithEagerRelationships();

    @Query("select p from Project p left join fetch p.users where p.id =:id")
    Project findOneWithEagerRelationships(@Param("id") Long id);

}
