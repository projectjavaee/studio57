package ru.ars.repository;

import ru.ars.domain.Authority;

import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Spring Data JPA repository for the Authority entity.
 */
public interface AuthorityRepository extends JpaRepository<Authority, Long> {

    Authority findByName(String name);
}
