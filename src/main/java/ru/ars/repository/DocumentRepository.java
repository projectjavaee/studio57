package ru.ars.repository;

import ru.ars.domain.Document;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;


/**
 * Spring Data JPA repository for the Document entity.
 */
@Repository
@SuppressWarnings("unused")
public interface DocumentRepository extends JpaRepository<Document, Long> {

}
