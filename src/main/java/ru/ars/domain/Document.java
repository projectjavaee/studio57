package ru.ars.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A Document.
 */
@Entity
@Table(name = "document")
@JsonIgnoreProperties({"date"})
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Document implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long id;

    private String name;

    private byte[] file;

    private String fileContentType;

    private LocalDate date;

    private Set<Project> projects = new HashSet<>();

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @NotNull
    @Column(name = "name", nullable = false)
    public String getName() {
        return name;
    }

    public Document name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Lob
    @NotNull
    @Column(name = "file", nullable = false)
    public byte[] getFile() {
        return file;
    }

    public Document file(byte[] file) {
        this.file = file;
        return this;
    }

    public void setFile(byte[] file) {
        this.file = file;
    }

    @Column(name = "file_content_type", nullable = false)
    public String getFileContentType() {
        return fileContentType;
    }

    public Document fileContentType(String fileContentType) {
        this.fileContentType = fileContentType;
        return this;
    }

    public void setFileContentType(String fileContentType) {
        this.fileContentType = fileContentType;
    }

    @Column(name = "date_load")
    @JsonProperty("date_load")
    public LocalDate getDate() {
        return date;
    }

    public Document dateLoad(LocalDate date) {
        this.date = date;
        return this;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    @ManyToMany
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    @JoinTable(name = "project_document",
        joinColumns = @JoinColumn(name="projects_id", referencedColumnName="id"),
        inverseJoinColumns = @JoinColumn(name="documents_id", referencedColumnName="id"))
    public Set<Project> getProjects() {
        return projects;
    }

    public Document projects(Set<Project> projects) {
        this.projects = projects;
        return this;
    }

    public Document addProject(Project project) {
        this.projects.add(project);
        project.getDocuments().add(this);
        return this;
    }

    public Document removeProject(Project project) {
        this.projects.remove(project);
        project.getDocuments().remove(this);
        return this;
    }

    public void setProjects(Set<Project> projects) {
        this.projects = projects;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Document document = (Document) o;
        if (document.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), document.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Document{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", fileContentType='" + fileContentType + "'" +
            ", date='" + getDate() + "'" +
            "}";
    }
}
