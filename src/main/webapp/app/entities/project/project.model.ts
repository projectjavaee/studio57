import { BaseEntity, User } from './../../shared';

export class Project implements BaseEntity {
    constructor(
        public id?: number,
        public name?: string,
        public site?: string,
        public date_create?: any,
        public description?: string,
        public logoContentType?: string,
        public logo?: any,
        public users?: User[],
    ) {
    }
}
