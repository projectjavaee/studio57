import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

import { Studio57ProjectModule } from './project/project.module';
import { Studio57DocumentModule } from './document/document.module';

@NgModule({
    imports: [
        Studio57ProjectModule,
        Studio57DocumentModule,
    ],
    declarations: [],
    entryComponents: [],
    providers: [],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class Studio57EntityModule {}
