import { BaseEntity } from './../../shared';

export class Document implements BaseEntity {
    constructor(
        public id?: number,
        public name?: string,
        public fileContentType?: string,
        public file?: any,
        public date_load?: any,
        public projects?: BaseEntity[],
    ) {
    }
}
