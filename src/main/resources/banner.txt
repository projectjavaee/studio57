
  ${AnsiColor.RED}    ███████████   █████${AnsiColor.BLUE}  ███ █████ █████ █████ █████ ═╗
  ${AnsiColor.RED}   ██  ████     ████     ██${AnsiColor.BLUE} ███ █████ █████ █████ █████ ╚═╗
  ${AnsiColor.RED}  ██   ████    ██  ███${AnsiColor.BLUE}        ███ █████ █████ █████ █████ ║
  ${AnsiColor.RED} ████████████        ███${AnsiColor.BLUE}    ███ █████ █████ █████ █████ ║
  ${AnsiColor.RED}██     ████   ███ ██     ██${AnsiColor.BLUE} ███ █████ █████ █████ █████ ╔═╝
  ${AnsiColor.RED}██     ████     ██  █████${AnsiColor.BLUE}  ███ █████ █████ █████ █████ ═╝

${AnsiColor.BRIGHT_YELLOW}:: Ruslan Sabirov 😉
${AnsiColor.BRIGHT_CYAN}:: Running Spring Boot ${spring-boot.version} ::
:: http://www.ars-spc.ru ::${AnsiColor.BLACK}
