import { browser, element, by, $ } from 'protractor';
import { NavBarPage } from './../page-objects/jhi-page-objects';
const path = require('path');

describe('Document e2e test', () => {

    let navBarPage: NavBarPage;
    let documentDialogPage: DocumentDialogPage;
    let documentComponentsPage: DocumentComponentsPage;
    const fileToUpload = '../../../../main/webapp/content/images/logo-jhipster.png';
    const absolutePath = path.resolve(__dirname, fileToUpload);
    

    beforeAll(() => {
        browser.get('/');
        browser.waitForAngular();
        navBarPage = new NavBarPage();
        navBarPage.getSignInPage().autoSignInUsing('admin', 'admin');
        browser.waitForAngular();
    });

    it('should load Documents', () => {
        navBarPage.goToEntity('document');
        documentComponentsPage = new DocumentComponentsPage();
        expect(documentComponentsPage.getTitle()).toMatch(/studio57App.document.home.title/);

    });

    it('should load create Document dialog', () => {
        documentComponentsPage.clickOnCreateButton();
        documentDialogPage = new DocumentDialogPage();
        expect(documentDialogPage.getModalTitle()).toMatch(/studio57App.document.home.createOrEditLabel/);
        documentDialogPage.close();
    });

    it('should create and save Documents', () => {
        documentComponentsPage.clickOnCreateButton();
        documentDialogPage.setNameInput('name');
        expect(documentDialogPage.getNameInput()).toMatch('name');
        documentDialogPage.setFileInput(absolutePath);
        documentDialogPage.setDate_loadInput('2000-12-31');
        expect(documentDialogPage.getDate_loadInput()).toMatch('2000-12-31');
        documentDialogPage.save();
        expect(documentDialogPage.getSaveButton().isPresent()).toBeFalsy();
    }); 

    afterAll(() => {
        navBarPage.autoSignOut();
    });
});

export class DocumentComponentsPage {
    createButton = element(by.css('.jh-create-entity'));
    title = element.all(by.css('jhi-document div h2 span')).first();

    clickOnCreateButton() {
        return this.createButton.click();
    }

    getTitle() {
        return this.title.getAttribute('jhiTranslate');
    }
}

export class DocumentDialogPage {
    modalTitle = element(by.css('h4#myDocumentLabel'));
    saveButton = element(by.css('.modal-footer .btn.btn-primary'));
    closeButton = element(by.css('button.close'));
    nameInput = element(by.css('input#field_name'));
    fileInput = element(by.css('input#file_file'));
    date_loadInput = element(by.css('input#field_date_load'));

    getModalTitle() {
        return this.modalTitle.getAttribute('jhiTranslate');
    }

    setNameInput = function (name) {
        this.nameInput.sendKeys(name);
    }

    getNameInput = function () {
        return this.nameInput.getAttribute('value');
    }

    setFileInput = function (file) {
        this.fileInput.sendKeys(file);
    }

    getFileInput = function () {
        return this.fileInput.getAttribute('value');
    }

    setDate_loadInput = function (date_load) {
        this.date_loadInput.sendKeys(date_load);
    }

    getDate_loadInput = function () {
        return this.date_loadInput.getAttribute('value');
    }

    save() {
        this.saveButton.click();
    }

    close() {
        this.closeButton.click();
    }

    getSaveButton() {
        return this.saveButton;
    }
}
